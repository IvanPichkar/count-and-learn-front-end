module.exports = {
  devServer: {
    proxy: {
      '/api': {
         target: 'https://wordstolearn.xyz',
        secure: false,
        pathRewrite: { '^/api': '' },
      },
    },
  },

  css: {
    loaderOptions: {
      less: {
        prependData: '@import "@/styles/material-dashboard/variables.less";',
      },
    },
  },

  pluginOptions: {
    i18n: {
      locale: 'ua',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: true,
    },
  },
};
