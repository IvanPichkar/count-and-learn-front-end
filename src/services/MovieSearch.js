import axios from 'axios';

export default class MovieSearch {
  static async getDetails(query) {
    const res = await axios.get(process.env.VUE_APP_GET_MOVIE_ENDPOINT, { params: query });
    return res.data;
  }
}
