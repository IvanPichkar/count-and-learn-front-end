import axios from 'axios';

class TextAnalyzer {
  static async getCounterResult(params) {
    try {
      const { data } = await axios.get(process.env.VUE_APP_GET_WORD_LIST_ENDPOINT, { params });
      return data;
    } catch (err) {
      throw Error(err.response.data.message);
    }
  }
}
export default TextAnalyzer;
