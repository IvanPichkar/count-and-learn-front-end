/* eslint-disable */
import axios from 'axios';

const state = {
  movies: [],
};

const getters = {
  allMovies: () => state.movies,
};

const actions = {
  async fetchMovies({ commit }, query) {
    const { data } = await axios.get(
      process.env.VUE_APP_GET_MOVIE_LIST_ENDPOINT,
      { params: { query } },
    );
    if (data && !data.length) {
      throw new Error();
    }
    commit('fetchMovies', data);
  },
};

const mutations = {
  fetchMovies: (state, movies) => { state.movies = movies; },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
