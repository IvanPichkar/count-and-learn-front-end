/* eslint-disable */
import Vuex from 'vuex';
import Vue from 'vue';
import * as Cookies from 'js-cookie';
import createPersistedState from 'vuex-persistedstate';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    movies: [],
  },
  getters: {
    allMovies: (state) => state.movies,
  },
  actions: {
    async fetchMovies({ commit }, query) {
      const { data } = await axios.get(`${process.env.VUE_APP_GET_MOVIE_LIST_ENDPOINT}`, { params: { query } });
      if (data && !data.length) {
        throw new Error();
      }
      commit('fetchMovies', data);
    },
  },
  mutations: {
    fetchMovies: (state, movies) => { state.movies = movies; },
  },
  plugins: [createPersistedState({
    storage: {
      getItem: (key) => Cookies.get(key),
      setItem: (key, value) => Cookies.set(key, value, { expires: 3, secure: true }),
      removeItem: (key) => Cookies.remove(key),
    },
  })],

  /* modules: {
    movies
  },
  plugins: [
    createPersistedState({
      storage: {
        getItem: (key) => Cookies.get(key),
        setItem: (key, value) =>
          Cookies.set(key, value, { expires: 3, secure: true }),
        removeItem: (key) => Cookies.remove(key),
      }
    }),
  ], */
});
