import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Landing from '../views/Landing.vue';
import Movie from '../views/Movie.vue';
import Signin from '../views/Signin.vue';
import Join from '../views/Join.vue';
import About from '../views/About.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/landing',
    name: 'Landing',
    component: Landing,
  },
  {
    path: '/mediatype/:mediatype/id/:id',
    name: 'Movie',
    component: Movie,
  },
  {
    path: '/signin',
    name: 'signin',
    component: Signin,
  },
  {
    path: '/join',
    name: 'join',
    component: Join,
  },
  {
    path: '/about',
    name: 'about',
    component: About,
  },
];

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes,
});

export default router;
